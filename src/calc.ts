export function sum(...nums: number[]) {
    return nums.reduce((total,num)=> total + num);
}

export function multiply(m:number, ...nums: number[]) {
    return nums.map((n)=> m * n);
}