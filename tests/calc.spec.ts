import { expect } from "chai";
import { sum, multiply} from "../src/calc";

describe("The calc module", ()=>{

    context(`#sum`, ()=> {

        it(`should exist`, ()=> {
            expect(sum).to.be.a("function");
            expect(sum).to.be.instanceOf(Function);
        });

        it(`should sum two numbers`, ()=> {
            const actual = sum(2,3);
            expect(actual).to.equal(5);
        });

        it(`should sum several numbers`, ()=> {
            const actual = sum(2,3,4,5,6);
            expect(actual).to.equal(20);
        });
    });

    context(`#multiply`, ()=> {

        it(`should exist`, ()=> {
            expect(multiply).to.be.a("function");
            expect(multiply).to.be.instanceOf(Function);
        });

        it(`should sum two numbers`, ()=> {
            const actual = multiply(2,3);
            expect(actual).to.eql([6]);
        });

        it(`should sum several numbers`, ()=> {
            const actual = multiply(2,3,4,5,6);
            expect(actual).to.eql([6,8,10,12]);
        });
    });
});